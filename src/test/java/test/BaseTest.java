package test;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

public class BaseTest {
        WebDriver driver;


        @BeforeSuite
        public void setupSuite(){

            String browserValue = System.getProperty("BROWSER");
                    switch (browserValue) {
                        case "CHROME":
                          WebDriverManager.chromedriver().setup();
                          break;
                        case "FIREFOX":
                            WebDriverManager.firefoxdriver().setup();
                            break;
                        default:
                            throw new IllegalArgumentException("Browser not supported");



            }

            WebDriverManager.chromedriver().setup();
    }


        @BeforeMethod
        public void setUp() {
            //se poate pune intr-o clasa parinte si dam extend
            this.driver = new ChromeDriver();
        }


        @AfterMethod
        public void cleanUp() {
            this.driver.quit();
        }
    }


