package test;

ackage test;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Set;

public class CookiesTest extends BaseTest {

    @Test
    public void checkOneCookie() {
        driver.get("http://www.google.com");
        Cookie c = driver.manage().getCookieNamed("CONSENT");
        Assert.assertNotNull(c, "Check if cookie is null");
    }

    @Test
    public void createCookie() {
        driver.get("http://google.com");
        Cookie myCookie = new Cookie("MyCookieName", "MyCookieValue");
        driver.manage().addCookie(myCookie);

        Cookie myCreatedCookie = driver.manage().getCookieNamed("MyCookieName");
        Assert.assertNotNull(myCreatedCookie);
    }

    @Test
    public void greenBtnTest() throws IOException {
        driver.get("http://siit.epizy.com/web-stubs/stubs/cookie.html?i=1");
        WebElement greenBtn = driver.findElement(By.id("set-cookie"));
        greenBtn.click();

        Cookie expectedCookie = driver.manage().getCookieNamed("gibberish");
        Assert.assertNotNull(expectedCookie);
    }

    public class deleteCookie {
        public static void main(String[] args) {
            WebDriver driver = new ChromeDriver();
            try {
                driver.get("http://siit.epizy.com/web-stubs/stubs/cookie.html?i=1");
                driver.manage().addCookie(new Cookie("gibberish", "cookie1"));
                Cookie cookie1 = new Cookie("test2", "cookie2");
                driver.manage().addCookie(cookie1);

                driver.manage().deleteCookieNamed("gibberish");

                driver.manage().deleteCookie(cookie1);
            } finally {
                driver.quit();
            }
        }
    }
}
