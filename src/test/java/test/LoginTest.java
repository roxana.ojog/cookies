package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.WelcomePage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
public class LoginTest {
    WebDriver driver;
    private final String URL = "http://siit.epizy.com/web-stubs/stubs/auth.html";
    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        driver.get(URL);
        login.Page = PageFactory.initElements(driver, login.Page.class);
    }
    @AfterMethod
    public void cleanUp() {
        this.driver.quit();
    }

    @DataProvider(name = "loginDataProvider")
        public Iterator<Object[]> loginDataProvider(){
            Collection<Object[]> dataProvider = new ArrayList<>();
            dataProvider.add(new String[] {"dinosaur", "dinosaurpassword"});
            dataProvider.add(new String[] {"dingo", "dingopassword"});
            dataProvider.add(new String[] {"camel", "camelpassword"});
            dataProvider.add(new String[] {"zebra", "zebrapassword"});
            return dataProvider.iterator();
        }
        @Test(dataProvider = "loginDataProvider")
        public void loginTest(String username, String password) {
        }
            //test cu data provider
            WelcomePage welcomePage = PageFactory.initElements(driver, WelcomePage.class);
            Assert.assertEquals(welcomePage.getUsernameElement(), username);


    @FindBy (how = How.XPATH, using = "/html/body/div/div[2]/div[1]/div/div[2]/form/div[2]/div/div/ul/li")
    private WebElement userErrorElement;
    @FindBy (how = How.XPATH, using = "//*[@id=\"login_form\"]/div[3]/div/div[1]/ul/li")
    private  WebElement passwordErrorElement;
    @DataProvider(name = "loginData")
    public Iterator<Object[]> loginData(){
        Collection<Object[]> dataProvider = new ArrayList<>();
        //empty username
        dataProvider.add(new String[] {" ", "dinosaurpassword"});
        //Empty password
        dataProvider.add(new String[] {"dingo", " "});
        //Empty username and password
        dataProvider.add(new String[] {" ", " "});
        //Valid username, unvalid password
        dataProvider.add(new String[] {"zebra", "blahblah"});
        //unvalid username, valid password
        dataProvider.add(new String[] {"blahblah", "zebrapassword"});
        //unnacepted combination
        dataProvider.add(new String[] {"zebra", "camelpassword"});
        return dataProvider.iterator();
    }
    @DataProvider(name = "loginUsernameData")
    public Iterator<Object[]> loginUsernameData(){
        Collection<Object[]> dataProvider = new ArrayList<>();
        //empty username
        dataProvider.add(new String[] {" ", "dinosaurpassword"});
        //Empty username and password
        dataProvider.add(new String[] {" ", " "});
        return dataProvider.iterator();
    }
    /*   @Test(dataProvider = "loginData")
       public void loginTest(String username, String password) {
           //test cu data provider
           LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
           WebElement myMessageMain = driver.findElement(By.id("login-error"));
           Assert.assertTrue(myMessageMain.isDisplayed());
       }
     */
    @Test(dataProvider = "loginUsername")
    public void usernameTest(String username, String password){
        LoginPage loginPageUsername = PageFactory.initElements(driver, LoginPage.class);
        WebElement myMessageUsername = driver.findElement(By.xpath("/html/body/div/div[2]/div[1]/div/div[2]/form/div[2]/div/div/ul/li)"));
        Assert.assertTrue(myMessageUsername.isDisplayed());
    }
}

