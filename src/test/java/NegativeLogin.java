import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.WelcomePage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class NegativeLogin {
    WebDriver driver;
    private final String URL = "http://siit.epizy.com/web-stubs/stubs/auth.html";

    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        driver.get(URL);
    }

    @AfterMethod
    public void cleanUp() {
        this.driver.quit();
    }

    @DataProvider(name = "loginUnsuccesfulDataProvider")
    public Iterator<Object[]> loginUnsuccesfulDataProvider() {
        Collection<Object[]> dataProvider = new ArrayList<>();
        dataProvider.add(new String[]{"dinosaur", "dinosaurpassword"});
        dataProvider.add(new String[]{"dingo", "dingopassword"});
        dataProvider.add(new String[]{"camel", "camelpassword"});
        dataProvider.add(new String[]{"zebra", "zebrapassword"});
        return dataProvider.iterator();
    }

    @Test(dataProvider = "loginDataProvider")
    public void loginTest(String username, String password) {
        //test cu data provider
        WelcomePage welcomePage = PageFactory.initElements(driver, WelcomePage.class);
        Assert.assertEquals(welcomePage.getUsernameElement(), username);

    }
}

