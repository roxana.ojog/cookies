package test;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class ActionsTest extends BaseTest {
    @Test

    public void hoverTest() {
        driver.get("http://siit.epizy.com/web-stubs/stubs/hover.html");
        WebElement hoverButton = driver.findElement(By.className("dropbtn"));

        Actions actions = new Actions(driver);
        actions = actions.moveToElement(hoverButton);
        Action action = actions.build();
        action.perform();
    }
    @Test
    public void testAll(){
        driver.get("http://siit.epizy.com/web-stubs/stubs/hover.html");

        List<WebElement> elements = driver.findElements(By.cssSelector("a.clickable"));

        for(WebElement item: elements){
            item.click();

            String name = item.getAttribute("name");
            String expectedText = "You clicked" + name;

            WebElement resultElement = driver.findElement(By.id("result"));
            String actualText = resultElement.getText();

            System.out.println("The text is now" + actualText);

            Assert.assertEquals(actualText, expectedText, "Checking text");
        }
}







}
